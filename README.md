<p align="center">
  <img width="512" height="256" src="https://github.com/Trigex/AlphaNET/blob/master/Assets/Images/Banners/alphanet_banner_512.png">
</p>

It's AlphaNET, but the 4th rewrite!

Hacking games are a very niche and underexplored genre. You’ve got the bigger games like Hacker Experience, Hacknet, Hackmud, and Dark Signs/Hacking Simulator. However, here’s an issue with all these games: it’s nothing like actual hacking.

They are essentially puzzle games, with a hacking/computer theme. Some have programming (they’re the better ones!), which does make them a bit more accurate and realistic, but they’re not too in-depth. Hackmud for example, makes heavy use of JavaScript scripting, but to accomplish what in the end? Solving puzzles to get more of the in game currency. Hacking Simulator had programming (in an awful, awful original language), but it was quite limited. You could program your given server project, and do things locally, but that was about it. All these limitations frustrate me! What I want in a hacking game is a nice, open sandbox, with a complete scripting engine and API, so I can write whatever the hell I want; I want hacking that’s more than just a simple puzzle; I want a complete, virtual operating system environment to play in to my heart's content!

And that’s the goal of AlphaNET. AlphaNET aims to be a complete sandbox, with a fully featured scripting API, realistic and open operating system internals, complete customization, and most importantly: actual hacking and cracking!
